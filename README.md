# GENERATE AND RESTORE POSTGRES DUMPS

Linux bash script used to import tables/data from an external database to your local database

## Requirements
-Local install postgresql and create user
-Close all postgresql connections(Temporal)
-If user error permission from postgres super user: ALTER USER role_specification WITH CREATEDB;
## Usage

```bash
sh dumps.sh # exec commands
```

## Environment example
.env
```
LOCAL_DB_HOST=
LOCAL_DB_NAME=
PG_DB_ADMIN=
LOCAL_DB_USER=
LOCAL_DB_PASSWORD=
REMOTE_DB_PASSWORD=
REMOTE_DB_HOST=
REMOTE_DB_USER=
REMOTE_DB_NAME=
RESTORE_DB_NAME=
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.