#!/bin/sh
now=$(date +"%F")
. ./.env
mkdir -p backup
psql -v LOCAL_DB_NAME=${LOCAL_DB_NAME} "dbname=${PG_DB_ADMIN} host=${LOCAL_DB_HOST} user=${LOCAL_DB_USER} password=${LOCAL_DB_PASSWORD}" < ./prev/dropCreateDb.sql
echo "STEP 1/4 \e[32mOK.\e[0m"
PGPASSWORD="${REMOTE_DB_PASSWORD}" pg_dump -h ${REMOTE_DB_HOST} -U ${REMOTE_DB_USER} ${REMOTE_DB_NAME} -O > ./backup/${RESTORE_DB_NAME}_$now.sql
echo "STEP 2/4 \e[32mOK.\e[0m"
psql --output=dumps.log "dbname=${LOCAL_DB_NAME}  host=${LOCAL_DB_HOST} user=${LOCAL_DB_USER} password=${LOCAL_DB_PASSWORD}" < ./backup/${RESTORE_DB_NAME}_$now.sql
echo "STEP 3/4 \e[32mOK.\e[0m"
psql --quiet "dbname=${LOCAL_DB_NAME} host=${LOCAL_DB_HOST} user=${LOCAL_DB_USER} password=${LOCAL_DB_PASSWORD}" < ./config/developConfig.sql
echo "STEP 4/4 \e[32mOK.\e[0m"